# smf-client

* The purpose of this project is make a proff of concept of client that uses the smf-client-lib which will make queries to the Entity API.
* This application represents the SMF BackEnd or other applications that need to consume from Sports Entities API and will use the smf-client-lib
```
+-----------+            +-----------+          +----------+
|    SMF    +----------->+    SMF    +--------->+   SMF    |
|    UI     |            |  BackEnd  |          | Mock API |
|           +<-----------+           +<---------+          |
+-----------+            +-----------+          +----------+

```

## Main Features
* Use smf-client-lib to Query games, Competitions and Teams

# Getting Started

## Requirements
* JDK 11
* Maven

## How to use
* Build and generate smf-client-lib jar
* Build application ```mvn clean install```
* Run the Application
* Schema can be seen using [voyager](http://localhost:8082/voyager) or [graphiql](http://localhost:8082/graphiql)
* Queries can be done in [graphiql](http://localhost:8082/graphiql)
