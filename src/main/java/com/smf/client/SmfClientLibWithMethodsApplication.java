package com.smf.client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SmfClientLibWithMethodsApplication {

	public static void main(String[] args) {
		SpringApplication.run(SmfClientLibWithMethodsApplication.class, args);
	}

}
