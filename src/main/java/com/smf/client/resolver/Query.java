package com.smf.client.resolver;

import com.smf.smfclientlibwithmethods.external.service.clients.CompetitionServiceGraphQLClient;
import com.smf.smfclientlibwithmethods.external.service.clients.GameServiceGraphQLClient;
import com.smf.smfclientlibwithmethods.external.service.clients.TeamServiceGraphQLClient;
import com.smf.smfclientlibwithmethods.model.BasketballCompetition;
import com.smf.smfclientlibwithmethods.model.BasketballGame;
import com.smf.smfclientlibwithmethods.model.BasketballTeam;
import graphql.kickstart.tools.GraphQLQueryResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collection;

@Component
public class Query implements GraphQLQueryResolver {

    @Autowired
    CompetitionServiceGraphQLClient competitionServiceGraphQLClient;
    @Autowired
    GameServiceGraphQLClient gameServiceGraphQLClient;
    @Autowired
    TeamServiceGraphQLClient teamServiceGraphQLClient;

//     Competition queries

    public BasketballCompetition competition(String competitionId) throws Exception {
        return competitionServiceGraphQLClient.getCompetitionById(competitionId);
    }

    public Collection<BasketballCompetition> competitions() throws Exception {
        return competitionServiceGraphQLClient.getAllCompetitions();
    }

//     Team queries

    public BasketballTeam team(String teamId) throws Exception {
        return teamServiceGraphQLClient.getTeamById(teamId);
    }

    public Collection<BasketballTeam> teams() throws Exception {
        return teamServiceGraphQLClient.getAllTeams();
    }

    // Game queries

    public BasketballGame game(String gameId) throws Exception {
        return gameServiceGraphQLClient.getBasketballGameById(gameId);
    }

    public Collection<BasketballGame> games() throws Exception {
        return gameServiceGraphQLClient.getAllCBasketballGames();
    }

}
