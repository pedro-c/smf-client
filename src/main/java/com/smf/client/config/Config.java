package com.smf.client.config;

import com.smf.smfclientlibwithmethods.external.service.clients.CompetitionServiceGraphQLClient;
import com.smf.smfclientlibwithmethods.external.service.clients.GameServiceGraphQLClient;
import com.smf.smfclientlibwithmethods.external.service.clients.TeamServiceGraphQLClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

@Configuration
public class Config {

    @Bean
    public CompetitionServiceGraphQLClient competitionServiceGraphQLClient() {
        return new CompetitionServiceGraphQLClient();
    }

    @Bean
    public GameServiceGraphQLClient gameServiceGraphQLClient() {
        return new GameServiceGraphQLClient();
    }

    @Bean
    public TeamServiceGraphQLClient teamServiceGraphQLClient() {
        return new TeamServiceGraphQLClient();
    }

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate(new HttpComponentsClientHttpRequestFactory());
    }
}
